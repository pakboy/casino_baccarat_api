require('dotenv').config();
var express = require('express'), app = express(), port = process.env.PORT || 3000;
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
require('./socket/index.js')(io);
var bodyParser = require('body-parser');
var router = express.Router();

app.use(express.static(__dirname + '/node_modules'));
app.get('/', function(req, res,next) {
    res.sendFile(__dirname + '/client.html');
});

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// requires different module routes
require('./routes/index.js')(router, io);

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

server.listen(port, function () {
    console.log('App Server is now running at:' + port);
});
