-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 29, 2017 at 01:31 PM
-- Server version: 5.7.18
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `online_casino`
--

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `uuid` varchar(250) DEFAULT NULL,
  `game_type` int(11) DEFAULT '1',
  `name` text,
  `max_players` int(11) DEFAULT NULL,
  `current_players` int(11) DEFAULT NULL,
  `max_bid` int(11) DEFAULT '4',
  `min_bid` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `uuid`, `game_type`, `name`, `max_players`, `current_players`, `max_bid`, `min_bid`, `is_active`, `created_at`, `updated_at`) VALUES
(1, '2d5c43f8-21cd-4d1d-ad59-2db8f13e2364', 1, 'Room A', 4, 0, 10000, 100, 1, '2017-09-27 00:48:20', NULL),
(2, 'a95dcb13-22bf-4189-bd6b-4dacd4c0e93b', 1, 'Room B', 4, 0, 15000, 500, 1, '2017-09-27 00:48:20', NULL),
(3, '70adb2a5-8531-4235-859e-522130682857', 1, 'Room C', 4, 0, 20000, 1000, 1, '2017-09-27 00:48:20', NULL),
(4, 'de55a563-cd4d-4850-917b-0e7abe828f42', 1, 'Room D', 4, 0, 50000, 1000, 1, '2017-09-27 00:48:20', NULL),
(5, 'c4ecc1d8-995c-41e9-8be6-760e67dd84c3', 1, 'Room E', 4, 0, 100000, 1000, 1, '2017-09-27 00:48:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `room_players`
--

CREATE TABLE `room_players` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `total_used_chips` int(11) DEFAULT NULL,
  `previous_chips` int(11) DEFAULT '0',
  `is_active` tinyint(1) DEFAULT '0',
  `is_playing` tinyint(1) DEFAULT '0',
  `is_player_turns` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `uuid` varchar(150) DEFAULT NULL,
  `user_type` int(10) UNSIGNED DEFAULT '2',
  `email` varchar(250) DEFAULT NULL,
  `username` varchar(250) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `full_name` varchar(250) DEFAULT NULL,
  `associated_wallet` varchar(250) DEFAULT NULL,
  `total_chips_count` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `uuid`, `user_type`, `email`, `username`, `password`, `full_name`, `associated_wallet`, `total_chips_count`, `created_at`, `updated_at`) VALUES
(1, 'b2faf3eb-45a0-4a3d-a548-60513d3b3cbf', 2, 'ullen.d.faustino@gmail.com', 'ullenfaustino', 'password1234', 'Ullen Faustino', 'dfsa3232dsfasdf23435768sazg', 0, '2017-09-06 00:00:00', '2017-09-06 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_players`
--
ALTER TABLE `room_players`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `room_players`
--
ALTER TABLE `room_players`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
