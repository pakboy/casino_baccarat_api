require('dotenv').config();

const blocktrail = require('blocktrail-sdk');
const client = blocktrail.BlocktrailSDK({apiKey: process.env.BLOCKTRAIL_API_KEY, apiSecret: process.env.BLOCKTRAIL_API_SECRET, network: "BTC", testnet: process.env.BLOCKTRAIL_IS_TEST});

module.exports = blocktrail;
module.exports.client = client;
