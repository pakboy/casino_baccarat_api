'use strict';
require('dotenv').config();
const nodemailer = require('nodemailer');

exports.sendMail = (mailOptions) => {
  let transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true, // true for 465, false for other ports
      auth: {
          user: process.env.EMAIL_USERNAME,
          pass: process.env.EMAIL_PASSWORD
      }
  });

  // setup email data with unicode symbols
  // let mailOptions = {
  //     from: '"Fred Foo 👻" <foo@blurdybloop.com>', // sender address
  //     to: 'bar@blurdybloop.com, baz@blurdybloop.com', // list of receivers
  //     subject: 'Hello ✔', // Subject line
  //     text: 'Hello world?', // plain text body
  //     html: '<b>Hello world?</b>' // html body
  // };

  if (process.env.EMAIL_STATUS === 'on') {
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log("sendMail", error);
        }
        console.log('Message sent: %s', info.messageId);
    });
  }
}
