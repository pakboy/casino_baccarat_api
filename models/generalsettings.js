var bookshelf = require('./../database/bookshelf.js');

const GeneralSettings = bookshelf.Model.extend({
  tableName: 'general_settings',
  hasTimestamps: true,
  softDelete: true
});

module.exports = GeneralSettings;
