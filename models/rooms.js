var bookshelf = require('./../database/bookshelf.js');

const Rooms = bookshelf.Model.extend({
  tableName: 'rooms',
  hasTimestamps: true,
  softDelete: true
});

module.exports = Rooms;
