var bookshelf = require('./../database/bookshelf.js');

const User = bookshelf.Model.extend({
  tableName: 'users',
  hasTimestamps: true,
  softDelete: true
});

module.exports = User;
