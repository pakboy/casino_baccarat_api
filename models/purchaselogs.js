var bookshelf = require('./../database/bookshelf.js');

const PurchaseLogs = bookshelf.Model.extend({
  tableName: 'purchase_logs',
  hasTimestamps: true,
  softDelete: true
});

module.exports = PurchaseLogs;
