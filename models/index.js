const _ = require('lodash');

const models = [
  'user',
  'rooms',
  'generalsettings',
  'purchaselogs'
];

function init () {
  models.forEach((name) => {
    module.exports[_.startCase(name).replace(/\s+/ig, '')] = require(`./${name}`)
  });
};

init();
