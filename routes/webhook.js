'use strict';
// var auth = require('./../middleware/auth.js');

module.exports = function(router, controller) {

  router.route('/:wallet_address').post(controller.webhook.handleWebhook);

  return router;
}
