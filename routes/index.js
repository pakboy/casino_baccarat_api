'use strict';
// var auth = require('./../middleware/auth.js');
var controller = require('./../controllers/index.js');

module.exports = function(router, io) {

  router.use('/user', require('./user')(router, controller, io));
  router.use('/webhook', require('./webhook')(router, controller));

};
