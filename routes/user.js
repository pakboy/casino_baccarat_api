'use strict';
// var auth = require('./../middleware/auth.js');

module.exports = function(router, controller, io) {

  router.route('/:id').get(controller.user.findUserById(io));
  router.route('/all/users').get(controller.user.findAllUser);
  router.route('/authenticate/login').post(controller.user.doLogin);
  router.route('/create/account').post(controller.user.createUser);
  router.route('/:id/buy/chip').post(controller.user.buyChips);

  return router;
}
