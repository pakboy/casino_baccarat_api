const dotenv = require('dotenv').config();

module.exports = {
  client: 'mysql',
  connection: {
    host     : process.env.HOST || '127.0.0.1',
    user     : process.env.USERNAME || 'root',
    password : process.env.PASSWORD || 'password',
    database : process.env.DB_NAME || 'online_casino',
    charset  : process.env.DB_Charset || 'utf8'
  }
};
