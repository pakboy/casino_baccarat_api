require('dotenv').config();
const createError = require('http-errors');
const dataProvider = require('./../models/index.js');
const blocktrail = require('./../middleware/blocktrail.js');
const blocktrail_client = require('./../middleware/blocktrail.js').client;
const uuidv4 = require('uuid/v4');
const Promise = require('promise');

exports.findUserById = function(io) {
  return (req, res, next) => {
    const { id } = req.params;
    return dataProvider.User.forge({id: id})
      .fetch()
      .then((user)=>{
        if (!user) {
          return res.send({error: {message: "User not found"}});
        }

        // io.emit('messages', user.toJSON());

        return res.send(user.toJSON());
      }).catch((err) => {
        return res.send({error: err});
      });
  }
};

exports.findAllUser = (req, res, next) => {
  const { id } = req.params;
  return dataProvider.User.forge()
    .fetchAll()
    .then((users)=>{
      res.send(users.toJSON());
    }).catch((err) => {
      return res.send({error: err});
    });
};

exports.doLogin = (req, res, next) => {
  const { email, password } = req.body;
  return dataProvider.User.forge({email: email, password: password})
    .fetch()
    .then((user)=>{
      if (!user) {
        return res.send({status: 400, message: 'User not found'});
      }
      res.send({status: 200, message: 'success', data: user.toJSON()});
    }).catch((err) => {
      return res.send({status: 500, error: err});
    });
};

exports.createUser = (req, res, next) => {
  const { email, full_name, username, password } = req.body;

  dataProvider.User.forge({email: email})
  .fetch().then((user) => {
    if (user) {
      return res.send({status: 500, message: 'Email already used.'});
    }

    return dataProvider.User.forge({
      email, full_name, username, password,
      'uuid' : uuidv4()
    }).save().then((userModel) => {
      blocktrail_client.initWallet(process.env.BLOCKTRAIL_COMPANY_WALLET_USERNAME, process.env.BLOCKTRAIL_COMPANY_WALLET_PASSWORD,
          function(err, wallet) {
            wallet.getNewAddress(function(err, address) {
              userModel.save({ associated_wallet: address }, { patch: true })
              .then((savedUser) => {
                const user_uuid = savedUser.get('uuid');
                const assoc_wallet = savedUser.get('associated_wallet');
                blocktrail_client.setupWebhook(process.env.BASE_URL + '/api/webhook/' + assoc_wallet, user_uuid, function(err, result) {
                  blocktrail_client.subscribeAddressTransactions(user_uuid, assoc_wallet, 1, function(err, result) {
                    res.send({status: 200, message: 'success', data: savedUser.toJSON()});
                  });
                });
              });
            });
          });
    });
  }).catch((err) => {
    return res.send({status: 500, error: err});
  });

};

exports.buyChips = (req, res, next) => {
  const user_id = req.params.id;
  const { chips_count } = req.body;

  blocktrail_client.initWallet(process.env.BLOCKTRAIL_COMPANY_WALLET_USERNAME, process.env.BLOCKTRAIL_COMPANY_WALLET_PASSWORD, function(err, wallet) {
      if (err) {
        return res.send({status: 500, error: err});
      }
      wallet.getBalance(function(err, confirmedBalance, unconfirmedBalance) {
          if (err) {
            return res.send({status: 500, error: err});
          }
          console.log('Balance: ', blocktrail.toBTC(confirmedBalance));
          console.log('Unconfirmed Balance: ', blocktrail.toBTC(unconfirmedBalance));

          dataProvider.Generalsettings.where('module_name', 'chip_peso')
          .fetch()
          .tap((chipPeso) => {
            return new Promise.all([
              dataProvider.Generalsettings.where('module_name','peso_bitcoin')
              .fetch().then((pesoBitcoin) => {
                chipPeso.set('peso_bitcoin', pesoBitcoin.get('module_value'));
              })
            ]);
          })
          .then((chipPeso) => {
            let chip_in_peso = chipPeso.get('module_value');
            let peso_in_bitcoin = chipPeso.get('peso_bitcoin');
            let buychips_amount = chips_count * chip_in_peso;
            let chips_bitcoin_amount = buychips_amount * peso_in_bitcoin;

            if (chips_bitcoin_amount > blocktrail.toBTC(confirmedBalance)) {
              console.log("Insufficient amount....");
              return res.send({status: 500, error: "Insufficient amount"});
            } else {
              console.log(chips_bitcoin_amount);
            }
          });
        }
      );
    })

};
