require('dotenv').config();
const createError = require('http-errors');
const dataProvider = require('./../models');
const uuidv4 = require('uuid/v4');
const mailer = require('./../middleware/mailer');
const fs = require('fs');
const path = require('path');
const blocktrail = require('./../middleware/blocktrail.js');

exports.handleWebhook = (req, res, next) => {

  const { wallet_address } = req.params;
  const { addresses,  } = req.body;
  const { hash } = req.body.data;
  let rcv_btc_amount = blocktrail.toBTC(addresses[wallet_address]);
  // console.log(wallet_address);
  // console.log(req.body);
  // console.log(rcv_btc_amount);
  // console.log(hash);

  getSettings.then((settings) => {

    let total_btc = rcv_btc_amount / settings.peso_in_bitcoin;
    let chips_count = total_btc / settings.chip_in_peso;
    chips_count = Math.round(chips_count);

    dataProvider.User.where('associated_wallet', wallet_address)
    .fetch()
    .then((user) => {
      if (!user) {
        return res.send({status: 500, error: "User not found."});
      }

      user.save({ total_chips_count: chips_count }, { patch: true })
      .then((savedUser) => {

        let purchase_logs = {
          uuid: uuidv4(),
          user_id: savedUser.get('id'),
          hash: hash,
          wallet_address: wallet_address,
          rcv_bitcoin_amount: rcv_btc_amount,
          chips_count: chips_count
        }

        dataProvider.Purchaselogs.forge(purchase_logs).save()
        .then((savedPurchaselogs) => {

          let mailOptions = {
              from: '"Queen Online Games" <queenonlinegames@gmail.com>', // sender address
              to: savedUser.get('email'), // bar@blurdybloop.com, baz@blurdybloop.com list of receivers
              subject: '[Queen Games Online] payment confirmation ' + hash, // Subject line
              text: 'received ' + rcv_btc_amount + "BTC for " + chips_count + " chips.", // plain text body
              html: '<p>Payment successfully received!</p>' // html body
          };
          mailer.sendMail(mailOptions);

          res.send(savedPurchaselogs.toJSON());
        });
      })
      .catch((err) => {
        return res.send({status: 500, error: err});
      });
    })
    .catch((err) => {
      return res.send({status: 500, error: err});
    });

  })
  .catch((err) => {
    return res.send({status: 500, error: err});
  });
};

const getSettings = new Promise((resolve, reject) => {
    dataProvider.Generalsettings.where('module_name', 'chip_peso')
    .fetch()
    .tap((chipPeso) => {
      return Promise.all([
        dataProvider.Generalsettings.where('module_name','peso_bitcoin')
        .fetch().then((pesoBitcoin) => {
          chipPeso.set('peso_bitcoin', pesoBitcoin.get('module_value'));
        })
      ]);
    })
    .then((chipPeso) => {
      let chip_in_peso = chipPeso.get('module_value');
      let peso_in_bitcoin = chipPeso.get('peso_bitcoin');

      resolve({
        chip_in_peso: parseInt(chip_in_peso),
        peso_in_bitcoin: parseFloat(peso_in_bitcoin)
      });
    }).catch((err) => reject(err));
  });
