const _ = require('lodash');
const Deck = require('card-deck');
const Cards = require('./card');
const Stopwatch = require('timer-stopwatch');
const dataProvider = require('./../models/index.js');

module.exports = function(io) {
  var rooms = [], players = [], used_cards = [], curr_player_cards = [], curr_banker_cards = [];
  var timer = {}, timer_delay = {}, cardDeck = {};
  var is_draw = false;
  var game_timer = 30000, timer_elay_interval = 3000;

  updateRooms();

  io.on('connection', function(socket) {
    console.log('Client connected with id = ', socket.id);
    console.log('[connection][Players] = ', players);

    socket.on('USER_CONNECT', function(){
      console.log('User connected');
      updatePlayersInRooms();
      socket.emit('ROOMS', { rooms : rooms });
    });

    socket.on('JOIN_ROOM', function(data) {
      console.log("[JOIN_ROOM]", data);
      var currentUser = Object.assign({}, data, {
        client_id : socket.id
      });
      socket.join(data.room_id);

      players.push(currentUser);
      io.sockets.in(data.room_id).emit("PLAYER_JOINED", currentUser);
      updatePlayersInRooms();
    });

    socket.on("PLAY", function(data){
      // console.log(data);
      getPlayer(data.user_id).playingStatus = data.playingStatus;

      io.sockets.in(data.room_id).emit("PLAYERS", { players: players });
      console.log(players);

      if (!getRoom(data.room_id).is_round_playing) {
        getRoom(data.room_id).is_round_playing = true;

        timer[getRoom(data.room_id).uuid].reset(game_timer);
        timer[getRoom(data.room_id).uuid].start();
      } else {
        console.log("is currently playing....");
      }
      // console.log(getRoom(data.room_id));
    })

    socket.on('GET_CARD', function(data) {
      console.log("[GET_CARD]", data);

      var drawnCard = cardDeck[getRoom(data.room_id).uuid].draw(data.card_count);
      var remaining_cards = cardDeck[getRoom(data.room_id).uuid].remaining();

      console.log("drawn cards...[Room " + data.room_id + "]", drawnCard);
      console.log("remaining... [Room " + data.room_id + "]", remaining_cards);

      io.sockets.in(data.room_id).emit("CARD_DECK", { cards : drawnCard, remaining_cards_on_deck : remaining_cards });

      if (remaining_cards < 2) {
        console.log("Card decks has been reset....[Room " + data.room_id + "]");
        cardDeck[getRoom(data.room_id).uuid].cards(Cards.cards);
        cardDeck[getRoom(data.room_id).uuid].shuffle();
        io.sockets.in(data.room_id).emit("CARD_DECK_RESET");
      }
    });

    socket.on('TIMER_DELAY', function(data){
        timer_delay[getRoom(data.room_id).uuid].start();
    });

    socket.on('disconnect', function() {
      console.log("on client disconnected.....  ", socket.id);
      players.forEach(function(data, i) {
        if (data.client_id === socket.id) {
          console.log("on client disconnected uuid....", data.user_id);
          players.splice(i, 1);
          socket.leave(data.room_id);

          updatePlayersInRooms();
          io.sockets.in(data.room_id).emit('ROOMS', { rooms : rooms });
          io.sockets.in(data.room_id).emit("PLAYERS", { players: players });

          if (playersInRoom(data.room_id).length == 0) {
            getRoom(data.room_id).is_round_playing = false;

            timer[getRoom(data.room_id).uuid].stop();
            timer[getRoom(data.room_id).uuid].reset(game_timer);

            cardDeck[getRoom(data.room_id).uuid].cards(Cards.cards);
            cardDeck[getRoom(data.room_id).uuid].shuffle();
          }
        }
      });
      console.log("[disconnect][players] --> ", players);
    });
  });

  function updateRooms() {
    dataProvider.Rooms.forge({is_active: 1})
    .fetchAll()
    .then((mRooms) => {
      rooms = mRooms.toJSON();
      rooms.forEach(function(room, i){
        room['round_timer'] = game_timer;
        room['is_round_playing'] = false;

        // set multiple instance of timer
        timer[getRoom(room.id).uuid] = new Stopwatch(game_timer);
        timer[getRoom(room.id).uuid].onTime(function(time){
            console.log("[timer][onTime][Room " + room.id + "]", time.ms);
            io.sockets.in(room.id).emit("TIMER", { remaining_time_ms : time.ms });
        }).onDone(function(){
            console.log("[timer][onAlmostdone][Room " + room.id + "] Timer is complete");
            getRoom(room.id).is_round_playing = false;

            if (playersInRoom(room.id).length > 0) {
              timer[getRoom(room.id).uuid].reset(game_timer);
            }
        });

        // set multiple instance of timer delayer
        timer_delay[getRoom(room.id).uuid] = new Stopwatch(timer_elay_interval);
        timer_delay[getRoom(room.id).uuid].onTime(function(time){
            console.log("[timer_delay][onTime][Room " + room.id + "]", time.ms);
            io.sockets.in(room.id).emit("TIMER_DELAY_RUNNING", { remaining_time_ms : time.ms });
        }).onDone(function(){
            console.log("[timer_delay][onAlmostdone][Room " + room.id + "] Timer is complete");
            timer_delay[getRoom(room.id).uuid].reset(timer_elay_interval);
            io.sockets.in(room.id).emit("TIMER_DELAY_DONE");
        });

        // set multiple instance of card Deck
        cardDeck[getRoom(room.id).uuid] = new Deck();
        cardDeck[getRoom(room.id).uuid].cards(Cards.cards);
        cardDeck[getRoom(room.id).uuid].shuffle();
      });
      // console.log("rooms", rooms);
    });
  }

  function updatePlayersInRooms() {
    rooms.forEach(function(room, i){
      rooms[i].current_players = playersInRoom(room.id).length;
    });
  }

  function getRoom(room_id) {
    return _.find(rooms, function(room){
      return room.id == room_id;
    });
  }

  function getPlayer(user_id) {
    return _.find(players, function(player){
      return player.user_id == user_id;
    });
  }

  function playersInRoom(room_id) {
    var room_players = _.filter(players, function(player){
      return player.room_id == room_id;
    });
    return room_players;
  }
}
